﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;        // Referece for the camera's target.
    public float smoothing = 5f;

    Vector3 offset;                 // Var to hold offset amount.

    void Start()
    {
        // sets offset amount.
        offset = transform.position - target.position;
    }

    void FixedUpdate()
    {
        // updates camera position every frame.
        Vector3 targetCamPos = target.position + offset;
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
    }
}
