﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    public float timeBetweenAttacks = 0.5f;     // The time between each attack.
    public int attackDamage = 10;               // The amount of health taken away per attack.
    float itTimer = 0f;                         // Timer to prevent tagging back when the player and enemy are still touching from last tag.
    bool startTimer = false;
    Animator anim;                              // Reference to the animator component.
    GameObject player;                          // Reference to the player GameObject.
    GameObject gameTimer;                       // Reference to the game timer GameObject.
    bool playerInRange;                         // Whether player is withtin the trigger collider and can be attacked.
    float timer;                                // Timer for counting up to the next attack.


    void Awake()
    {
        // Setting up the references.
        player = GameObject.FindGameObjectWithTag("Player");
        gameTimer = GameObject.FindGameObjectWithTag("GameTimer");
        anim = GetComponent<Animator>();
    }

    void OnTriggerEnter(Collider other)
    {
        // If entering collider is the player...
        if (other.gameObject == player && !gameTimer.GetComponent<GameTimer>().gameOver)
        {   // if not it, make it..         
            if (GetComponent<EnemyMovement>().it == true && itTimer <= 0)
            {
                GetComponent<EnemyMovement>().it = false;
                itTimer = 1f;
                startTimer = true;
            }
            // else make not it.
            else if (GetComponent<EnemyMovement>().it == false && itTimer <= 0)
            {
                GetComponent<EnemyMovement>().it = true;
                itTimer = 1f;
                startTimer = true;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        // checks the distance between the player and enemy
        if (Vector3.Distance(player.transform.position, transform.position) < 1f)
        {
            // resets the movement selection timer in enemy movement script to 0 when too close to player.
            GetComponent<EnemyMovement>().choiceTimer = 0;
        }

        // Add time since Update was last called to the timer.
        timer += Time.deltaTime;
        if (startTimer == true)
        {
            itTimer -= Time.deltaTime;
        }        
    }
}
