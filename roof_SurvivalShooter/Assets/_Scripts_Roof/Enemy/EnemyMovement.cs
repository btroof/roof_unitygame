﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour
{
    // Speed modifier vars for pickups.
    public float speedSpeed = 9f;
    public float slowSpeed = 3f;  

    // Bool to determine who is it.
    public bool it;
    
    // Timers
    public float choiceTimer = 0.1f;
    public float newChoiceTimer = 0f;
    public float timer;
    public float pickUpEffectTimer = 4f;

    // Var to randomly select player or enemy for pick up effect to impact.
    public float pickUpImpact;

    // Bools to determine if pick ups were encountered.
    public bool pickedUpFreeze = false;
    public bool pickedUpSpeed = false;
    public bool pickedUpSlow = false;

    // References to game objects.
    public GameObject freezeParticle;
    public GameObject slowParticle;
    public GameObject speedParticle;
    GameObject playerObject;
    GameObject timeText;
    Transform player;
    NavMeshAgent nav;
    public Transform[] runToPoints;

    // Start is called before the first frame update
    void Awake()
    {
        timer = pickUpEffectTimer;
        it = true;

        // Setting up the references.
        player = GameObject.FindGameObjectWithTag("Player").transform;
        timeText = GameObject.FindGameObjectWithTag("GameTimer");
        playerObject = GameObject.FindGameObjectWithTag("Player");
        nav = GetComponent<NavMeshAgent>();
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Checks for gameover.
        if (timeText.GetComponent<GameTimer>().countdownTime > 0.1)
        {
            // Checks for an encounter with a pickup.
            if (pickedUpFreeze || pickedUpSpeed || pickedUpSlow)
            {
                // decrements timer for the pickup effect.
                timer -= Time.deltaTime;

                // if freeze, enemy is unable to move.
                if (pickedUpFreeze)
                {
                    nav.speed = 0;
                }

                // if speed, enemy move speed is increased.
                if (pickedUpSpeed)
                {
                    nav.speed = speedSpeed;
                }

                // if slow, enemy move speed is decreased
                if (pickedUpSlow)
                {
                    nav.speed = slowSpeed;
                }


                // resets pick up and movement states and timer value when timer expires.
                if (timer <= 0)
                {
                    if (it)
                    {
                        nav.speed = 6.25f;
                    }
                    else if (!it)
                    {
                        nav.speed = 6.75f;
                    }
                    pickedUpFreeze = false;
                    pickedUpSpeed = false;
                    pickedUpSlow = false;
                    pickUpImpact = 0;
                    timer = pickUpEffectTimer;
                }
            }

            // Checks for movement location choice timer to expire..
            if (choiceTimer <= 0)
            {
                // selects and sets new destination.
                int runToPointsIndex = Random.Range(0, runToPoints.Length);
                nav.SetDestination(runToPoints[runToPointsIndex].position);

                // resets timer to a random value.
                choiceTimer = Random.Range(0.3f, 2.5f);
            }

            // If the enemy is it....
            if (it)
            {
                // ...set to chase player.
                nav.SetDestination(player.position);
                
                // ...decrease acceleration..
                nav.acceleration = 10f;

                // Resets movement speed when enemy is not it.
                if (!pickedUpSpeed && !pickedUpSlow && !pickedUpFreeze)
                {
                    nav.speed = 6.25f;
                }
            }

            // If the enemy is not it and the player is too close
            // and the choice timer has not expired
            // and the enemy is not being impacted by a pickup...
            if (!it && Vector3.Distance(player.transform.position, transform.position) <= 25 && newChoiceTimer > 0 && !pickedUpSpeed && !pickedUpSlow && !pickedUpFreeze)
            {
                // ... Set the choice timer to 0
                choiceTimer = 0;
                
                // Increase acceleration.
                nav.acceleration = 18;

                // ... Increase speed to get away.
                nav.speed = 9f;
            }

            // If not it and the player is too close...
            if (!it && Vector3.Distance(player.transform.position, transform.position) > 25)
            {
                // decrease acceleration.
                nav.acceleration = 10;

                // if not impacted by a pick up...
                if (!pickedUpSpeed && !pickedUpSlow && !pickedUpFreeze)
                {
                    // slow back down.
                    nav.speed = 6.75f;
                }
            }

            // if not it...
            if (!it && choiceTimer >= 0)
            {
                // decrement choice timer.
                choiceTimer -= Time.deltaTime;
            }
        }

        // If the game is over...
        else
        {
            // stop.
            nav.speed = 0f;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        // randomly selects the player or enemy for pick up effect. 
        pickUpImpact = Random.Range(0f, 2f);

        // Checks object type.. 
        if (other.gameObject.CompareTag("PickUp_Speed") && !pickedUpSpeed && !pickedUpSlow && !pickedUpFreeze)
        {
            Destroy(other.gameObject);

            // ... spawns speed particle effect if speed pickup is encountered...
            Instantiate(speedParticle, other.transform.position, speedParticle.transform.rotation);

            if (pickUpImpact >= 1 && pickUpImpact <= 2)
            {
                // ... sets pickUpSpeed var to true if the enemy is chosen for its impact.
                pickedUpSpeed = true;
            }
            else
            {
                // ... else it impacts the player.
                playerObject.GetComponent<PlayerMovement>().pickedUpSpeed = true;
            }
        }

        // Checks object type.. 
        if (other.gameObject.CompareTag("PickUp_Slow") && !pickedUpSpeed && !pickedUpSlow && !pickedUpFreeze)
        {
            Destroy(other.gameObject);

            // ... spawns slow particle effect if slow pickup is encountered...
            Instantiate(slowParticle, other.transform.position, slowParticle.transform.rotation);
                       
            if (pickUpImpact >= 1 && pickUpImpact <= 2)
            {
                // ... sets pickedUpSlow var to true if the enemy is chosen for its impact.
                pickedUpSlow = true;
            }
            else
            {
                // ... else it impacts the player.
                playerObject.GetComponent<PlayerMovement>().pickedUpSlow = true;
            }
        }

        // Checks object type.. 
        if (other.gameObject.CompareTag("PickUp_Freeze") && !pickedUpSpeed && !pickedUpSlow && !pickedUpFreeze)
        {
            Destroy(other.gameObject);

            // ... spawns freeze particle effect if freeze pickup is encountered...
            Instantiate(freezeParticle, other.transform.position, freezeParticle.transform.rotation);

            if (pickUpImpact >= 1 && pickUpImpact <= 2)
            {
                // ... sets pickedUpFreeze var to true if the enemy is chosen for its impact.
                pickedUpFreeze = true;
            }
            else
            {
                // ... else it impacts the player.
                playerObject.GetComponent<PlayerMovement>().pickedUpFreeze = true;
            }
        }
    }
}

