﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameOverManager : MonoBehaviour
{
    GameObject enemyObject;
    GameObject timeText;
    GameObject winText;

    Animator anim;                          // Reference to the animator component.
    float restartTimer;                     // Timer to count up to restarting the level


    void Awake()
    {
        
        // Set up the references.
        anim = GetComponent<Animator>();
        enemyObject = GameObject.FindGameObjectWithTag("Enemy");
        timeText = GameObject.FindGameObjectWithTag("GameTimer");
        winText = GameObject.FindGameObjectWithTag("WinText");
    }

    // Update is called once per frame
    void Update()
    {
        // If time has expired...
        if (timeText.GetComponent<GameTimer>().gameOver == true)
        {
            // ... tell the animator the game is over.
            anim.SetTrigger("GameOver");

            // If the player is not it...
            if (enemyObject.GetComponent<EnemyMovement>().it)
            {
                // Display win text.
                winText.GetComponent<Text>().text = "You WIN!";
            }
            else
            {
                // Else display the lose text.
                winText.GetComponent<Text>().text = "You LOSE!";
            }
        }
    }
}
