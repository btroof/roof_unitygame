﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDestroyTimer : MonoBehaviour
{
    public float timeTillDestroy;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroyMe", timeTillDestroy);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void DestroyMe()
    {
        Destroy(gameObject);
    }
}
