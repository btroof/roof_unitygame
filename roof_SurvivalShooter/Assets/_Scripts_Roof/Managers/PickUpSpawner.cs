﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpSpawner : MonoBehaviour
{
    // Timers...
    public float waitToSpawn = 0f;
    float spawnDelay;
    bool afterStart = false;
    // References to game objects.
    public GameObject[] pickUpPrefeabs;
    GameObject spawnedPickup;

    // Start is called before the first frame update
    void Start()
    {
        // sets spwan delay to a random number.
        spawnDelay = Random.Range(0f, 10f);
    }

    // Update is called once per frame
    void Update()
    {
        // Decrements delay timer.
        if (spawnDelay >= 0f)
        {
            spawnDelay -= Time.deltaTime;
        }

        // delays first pick up spawn until timer is expired.
        if (spawnDelay <= 0 && !afterStart)
        {
            SpawnPickUp();
            afterStart = true;
        }

        // If something is not spawned...
        if (spawnedPickup == null)
        {
            // ... and the wait timer has expired...
            if (waitToSpawn <= 0f)
            {
                //... spawn a pickup
                SpawnPickUp();
            }
            else
            {
                // Otherwise decrement the wait timer.
                waitToSpawn -= Time.deltaTime;
            }
        }
    }

    // Function that handles pick up spawning.
    void SpawnPickUp()
    {
        // Sets reference to pick up randomly.
        GameObject toSpawn = pickUpPrefeabs[(int)(Random.value * pickUpPrefeabs.Length)];

        // Spawns pickup.
        spawnedPickup = Instantiate(toSpawn, transform.position, toSpawn.transform.rotation);

        // Sets random wait time.
        waitToSpawn = Random.Range(10f, 20f);
    }
}
