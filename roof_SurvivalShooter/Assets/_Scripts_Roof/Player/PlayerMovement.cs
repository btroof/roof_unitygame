﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    // Vars for setting/ resetting speed.
    public float speed = 6f;
    float currentSpeed;

    // Var to hold movement data.
    Vector3 movement;
    // Speed modifier vars for pickups. 
    public float speedSpeed = 9f;
    public float slowSpeed = 3f;

    // Timers
    public float pickUpEffectTimer = 4f;
    public float timer;

    // Var to randomly select player or enemy for pick up effect to impact.
    public float pickUpImpact;

    // Bools to determine if pick ups were encountered.
    public bool pickedUpFreeze = false;
    public bool pickedUpSpeed = false;
    public bool pickedUpSlow = false;

    // References to game objects.
    GameObject enemyObject;
    GameObject timeText;
    GameObject ItText;
    GameObject effectText;
    public GameObject freezeParticle;
    public GameObject slowParticle;
    public GameObject speedParticle;
    Transform enemy; 
    Animator anim;
    Rigidbody playerRigidbody;

    void Awake()
    {
        // Setting up references.
        effectText = GameObject.FindGameObjectWithTag("EffectText");
        enemyObject = GameObject.FindGameObjectWithTag("Enemy");
        enemy = GameObject.FindGameObjectWithTag("Enemy").transform;
        timeText = GameObject.FindGameObjectWithTag("GameTimer");
        ItText = GameObject.FindGameObjectWithTag("ItText");
        anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();

        // Initialize pickUpEffectTimer and currentSpeed vars 
        timer = pickUpEffectTimer;
        currentSpeed = speed;
    }

    void FixedUpdate()
    {
        // gets input from arrow keys 
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        // calls move function if game is not over.
        if (timeText.GetComponent<GameTimer>().countdownTime > 0.1)
        {
            Move(h, v);
        }

        // animates player when moving
        Animating(h, v);

        // makes player always look at the enemy.
        transform.LookAt(enemy.position);
    }

    private void Update()
    {
        // Checks for an encounter with a pickup.
        if (pickedUpFreeze || pickedUpSpeed || pickedUpSlow)
        {
            // decrements timer for the pickup effect.
            timer -= Time.deltaTime;

            // if freeze, player is unable to move and a message is displayed.
            if (pickedUpFreeze)
            {
                effectText.GetComponent<Text>().text = "You're Frozen!";
                RidgidbodyConstraints();
            }

            // if speed, player move speed is increased and a message is displayed.
            if (timer > 0f && pickedUpSpeed)
            {
                effectText.GetComponent<Text>().text = "You're Accelerated!";
                currentSpeed = speedSpeed;
            }

            // if slow, player move speed is decreased and a message is displayed.
            if (timer > 0f && pickedUpSlow)
            {
                effectText.GetComponent<Text>().text = "You're Slowed";
                currentSpeed = slowSpeed;
            }

            // resets pick up states and timer value when timer expires.
            if (timer <= 0)
            {
                effectText.GetComponent<Text>().text = " ";
                playerRigidbody.constraints = RigidbodyConstraints.FreezePositionY;
                pickedUpSpeed = false;
                pickedUpFreeze = false;
                pickedUpSlow = false;
                currentSpeed = speed;
                timer = pickUpEffectTimer;
                pickUpImpact = 0;
            }
        }


        if (enemyObject.GetComponent<EnemyMovement>().it == false)
        {           
            ItText.GetComponent<Text>().text = "You're IT!";
            if (timer == 2f && !pickedUpSpeed && !pickedUpSlow && !pickedUpFreeze)
            {
                speed = 6f;
            }
        }
        else
        { 
            ItText.GetComponent<Text>().text = " ";
            if (timer == 2f && !pickedUpSpeed && !pickedUpSlow && !pickedUpFreeze)
            {
                speed = 5.5f;
            }
        }

    }
    void Move(float h, float v)
    {
        movement.Set(h, 0f, v);
        movement = movement.normalized * currentSpeed * Time.deltaTime;
        playerRigidbody.MovePosition(transform.position + movement);

    }

    // Function that uses player walking animation when true to bring out of idle state.
    void Animating(float h, float v)
    {
        bool walking = h != 0f || v != 0f;
        anim.SetBool("IsWalking", walking);
    }

    // Function to detect pick up encounters 
    void OnTriggerEnter(Collider other)
    {
        // randomly selects the player or enemy for pick up effect. 
        pickUpImpact = Random.Range(0f, 2f);
        
        // Checks object type.. 
        if (other.gameObject.CompareTag("PickUp_Speed") && !pickedUpSpeed && !pickedUpSlow && !pickedUpFreeze)
        {
            Destroy(other.gameObject);

            // ... spawns speed particle effect if speed pickup is encountered...
            Instantiate(speedParticle, other.transform.position, speedParticle.transform.rotation);
            if (pickUpImpact >=0 && pickUpImpact < 1)
            {
                // ... sets pickUpSpeed var to true if the player is chosen for its impact.
                pickedUpSpeed = true;
            }
            else
            {
                // ... else it impacts the enemy.
                enemy.GetComponent<EnemyMovement>().pickedUpSpeed = true;
            }
        }

        // Checks object type.. 
        if (other.gameObject.CompareTag("PickUp_Slow") && !pickedUpSpeed && !pickedUpSlow && !pickedUpFreeze)
        {
            Destroy(other.gameObject);

            // ... spawns slow particle effect if slow pickup is encountered...
            Instantiate(slowParticle, other.transform.position, slowParticle.transform.rotation);
            if (pickUpImpact >= 0 && pickUpImpact < 1)
            {
                // ... sets pickUpSlow var to true if the player is chosen for its impact.
                pickedUpSlow = true;
            }
            else
            {
                // ... else it impacts the enemy.
                enemy.GetComponent<EnemyMovement>().pickedUpSlow = true;
            }
        }
        
        // Checks object type.. 
        if (other.gameObject.CompareTag("PickUp_Freeze") && !pickedUpSpeed && !pickedUpSlow && !pickedUpFreeze)
        {
            Destroy(other.gameObject);

            // ... spawns freeze particle effect if freeze pickup is encountered...
            Instantiate(freezeParticle, other.transform.position, freezeParticle.transform.rotation);
            if (pickUpImpact >= 0 && pickUpImpact < 1)
            {
                // ... sets pickUpfreeze var to true if the player is chosen for its impact.
                pickedUpFreeze = true;
            }
            else
            {
                // ... else it impacts the enemy.
                enemy.GetComponent<EnemyMovement>().pickedUpFreeze = true;
            }

        }
    }

    // Function to disable player movement.
    void RidgidbodyConstraints()
    {
        playerRigidbody.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;        
    }
}
